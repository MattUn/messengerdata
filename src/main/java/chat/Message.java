package chat;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import init.StartPoint;
import util.EncodingFix;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class Message{
    private final Chat containingChat;

    private final boolean isCurrentParticipant;
    private final boolean isUser;

    private final String sender;
    private final Instant time;
    private final String content;

    private final List<String> photoPaths;
    private final String stickerPath;
    private final List<ReactStore> reacts;
    private final String shareLink;
    private final List<VideoStore> videos;
    private final List<String> filePaths;
    private final Type type;

    public Message(Chat containingChat, JsonObject jsonMsg){
        this.containingChat=containingChat;
        if(jsonMsg.has("sender_name")){
            sender=EncodingFix.fixEncoding(jsonMsg.get("sender_name").getAsString());
        }else{
            sender=null;
        }
        if(jsonMsg.has("timestamp_ms")){
            time=Instant.ofEpochMilli(jsonMsg.get("timestamp_ms").getAsLong());
        }else{
            time=null;
        }
        if(jsonMsg.has("content")){
            content=EncodingFix.fixEncoding(jsonMsg.get("content").getAsString());
        }else{
            content="";
        }

        if(sender!=null){
            isUser=sender.equalsIgnoreCase(init.StartPoint.getUserName());
            isCurrentParticipant=containingChat.getCurrentParticipants().contains(sender);
        }else{
            isUser=false;
            isCurrentParticipant=false;
        }

        photoPaths=new ArrayList<>();
        if(jsonMsg.has("photos")){
            for(JsonElement element:jsonMsg.get("photos").getAsJsonArray()){
                photoPaths.add(StartPoint.getMsgDir().getAbsolutePath()+File.separator+
                        element.getAsJsonObject().get("uri").getAsString().substring(9));
            }
        }

        filePaths=new ArrayList<>();
        if(jsonMsg.has("files")){
            for(JsonElement element:jsonMsg.get("files").getAsJsonArray()){
                filePaths.add(StartPoint.getMsgDir().getAbsolutePath()+File.separator+
                        element.getAsJsonObject().get("uri").getAsString().substring(9));
            }
        }

        if(jsonMsg.has("sticker")){
                stickerPath=StartPoint.getMsgDir().getAbsolutePath()+File.separator+
                        jsonMsg.get("sticker").getAsJsonObject().getAsJsonObject().get("uri").getAsString().substring(9);
        }else{
            stickerPath="";
        }

        reacts=new ArrayList<>();
        if(jsonMsg.has("reactions")){
            for(JsonElement element:jsonMsg.get("reactions").getAsJsonArray()){
                JsonObject object=element.getAsJsonObject();
                reacts.add(new ReactStore(EncodingFix.fixEncoding(object.get("reaction").getAsString()),
                        EncodingFix.fixEncoding(object.get("actor").getAsString())));
            }
        }

        if(jsonMsg.has("share")&&jsonMsg.get("share").getAsJsonObject().has("link")){
            shareLink=jsonMsg.get("share").getAsJsonObject().get("link").getAsString();
        }else{
            shareLink="";
        }

        videos=new ArrayList<>();
        if(jsonMsg.has("videos")){
            for(JsonElement element:jsonMsg.get("videos").getAsJsonArray()){
                videos.add(new VideoStore(
                        StartPoint.getMsgDir().getAbsolutePath()+File.separator+
                                element.getAsJsonObject().get("uri").getAsString().substring(9),
                        StartPoint.getMsgDir().getAbsolutePath()+File.separator+
                                element.getAsJsonObject().get("thumbnail").getAsJsonObject().get("uri").getAsString().substring(9)));
            }
        }

        if(content.isEmpty()&&(photoPaths.size()!=0||videos.size()!=0||filePaths.size()!=0)){//only if no message as well
            type=Type.MEDIA_ONLY;
        }else if(!stickerPath.isEmpty()){//never any message
            type=Type.STICKER;
        }else if(content.equals("This poll is no longer available.")){
            type=Type.POLL_OLD;
        }else if(content.contains(" created a poll: ")){
            type=Type.POLL_CREATE;
        }else if(content.endsWith(" in the poll.")&&content.contains(" voted for ")){
            type=Type.POLL_RESPONSE;
        }else{
            type=Type.MESSAGE;
        }

    }


    public class ReactStore{
        private final String react;
        private final String actor;
        public ReactStore(String react, String actor){
            this.react=react;
            this.actor=actor;
        }
        public String getReact(){
            return react;
        }
        public String getActor(){
            return actor;
        }
        @Override
        public String toString(){
            return react+"-"+actor;
        }
    }

    public class VideoStore{
        private final String videoUri;
        private final String thumbUri;
        public VideoStore(String videoUri, String thumbUri){
            this.videoUri=videoUri;
            this.thumbUri=thumbUri;
        }
        public String getVideoUri(){
            return videoUri;
        }
        public String getThumbUri(){
            return thumbUri;
        }
    }

    public enum Type{
        MESSAGE,
        MEDIA_ONLY,
        POLL_OLD,
        POLL_CREATE,
        POLL_RESPONSE,
        STICKER
    }

    public boolean isCurrentParticipant(){
        return isCurrentParticipant;
    }

    public boolean isUser(){
        return isUser;
    }

    public String getSender(){
        return sender;
    }

    public Instant getTime(){
        return time;
    }

    public String getContent(){
        return content;
    }

    public List<String> getPhotoPaths(){
        return photoPaths;
    }

    public String getReactString(){
        StringBuilder builder=new StringBuilder();
        for(ReactStore react:reacts){
            builder.append(react);
            builder.append("   ");
        }
        return builder.toString();
    }

    public List<ReactStore> getReacts(){
        return reacts;
    }

    public String getShareLink(){
        return shareLink;
    }

    public String getStickerPath(){
        return stickerPath;
    }

    public List<VideoStore> getVideos(){
        return videos;
    }

    public List<String> getFilePaths(){
        return filePaths;
    }

    public Chat getContainingChat(){
        return containingChat;
    }

    public Type getType(){
        return type;
    }
}
