package chat;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import util.EncodingFix;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Chat implements Comparable<Chat>{
    private final List<String> participants;
    private List<Message> messages;
    private final String title;
    private final String filePath;
    private final boolean userParticipant;
    private final Section section;

    @Override
    public int compareTo(Chat o){//orders by time of recent message descending
        return messages.get(0).getTime().compareTo(o.messages.get(0).getTime());
    }


    public enum Section{
        INBOX,
        ARCHIVE
    }

    public Chat(JsonObject baseObj){
        title=EncodingFix.fixEncoding(baseObj.get("title").getAsString());

        filePath=baseObj.get("thread_path").getAsString();

        userParticipant=baseObj.get("is_still_participant").getAsBoolean();

        if(filePath.startsWith("inbox")){
            section=Section.INBOX;
        }else{
            section=Section.ARCHIVE;
        }

        participants=new ArrayList<>();
        JsonArray jsonParticipants=baseObj.get("participants").getAsJsonArray();
        for(JsonElement person:jsonParticipants){
            participants.add(EncodingFix.fixEncoding(person.getAsJsonObject().get("name").getAsString()));
        }

        messages=new ArrayList<>();
        JsonArray jsonMessages=baseObj.get("messages").getAsJsonArray();
        for(JsonElement message:jsonMessages){
            messages.add(new Message(this,message.getAsJsonObject()));
        }
    }

    public void addExtraFile(JsonObject baseObj){
        JsonArray jsonMessages=baseObj.get("messages").getAsJsonArray();
        for(JsonElement message:jsonMessages){
            messages.add(new Message(this,message.getAsJsonObject()));
        }
    }

    public List<String> getCurrentParticipants(){
        return participants;
    }

    public Set<String> getCombinedParticipants(){
        Set<String> cpart=new HashSet<>();
        for(Message message:messages){
            cpart.add(message.getSender());
        }
        return cpart;
    }

    public List<Message> getMessages(){
        return messages;
    }

    public String getTitle(){
        return title;
    }

    public String getFilePath(){
        return filePath;
    }

    public boolean isUserParticipant(){//appears to be same as archive
        return userParticipant;
    }

    public Section getSection(){
        return section;
    }
}
