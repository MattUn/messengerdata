package view;

import chat.Message;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class MessageBox extends JPanel{

    public MessageBox(Message message){
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        Border border;
        if(message.isUser()){
            border=BorderFactory.createLineBorder(Color.BLUE, 1);
        }else{
            border=BorderFactory.createLineBorder(Color.GRAY, 1);
        }
        setBorder(border);

        JPanel senderTimePanel=new JPanel();
        senderTimePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        add(senderTimePanel);

        JLabel lblSender=new JLabel(message.getSender()+"     ");
        lblSender.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
        if(!message.isCurrentParticipant()){
            lblSender.setForeground(Color.RED);
        }
        senderTimePanel.add(lblSender);

        DateTimeFormatter dtformatter=DateTimeFormatter.ofPattern("EEE MMM dd yyyy  HH:mm:ss").withZone(ZoneId.systemDefault());
        JLabel lblTime=new JLabel(dtformatter.format(message.getTime()));
        lblTime.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
        senderTimePanel.add(lblTime);

        JTextArea textArea=new JTextArea(message.getContent());
        textArea.setFont(new Font(Font.SANS_SERIF,Font.PLAIN,12));
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setOpaque(false);
        textArea.setEditable(false);
        textArea.setColumns(70);
        add(textArea);

        if(!message.getStickerPath().isEmpty()){
            JPanel stickerPan=new JPanel();
            stickerPan.setLayout(new FlowLayout(FlowLayout.LEFT));
            ImagePanel img=new ImagePanel(new File(message.getStickerPath()),80,false);
            stickerPan.add(img);
            add(stickerPan);
        }

        if(message.getPhotoPaths().size()>0){
            JPanel lblPan=new JPanel();
            lblPan.setLayout(new FlowLayout(FlowLayout.LEFT));
            JLabel lbl=new JLabel("Images: ");
            lbl.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
            lblPan.add(lbl);
            add(lblPan);
        }
        for(String imagePath:message.getPhotoPaths()){
            JPanel imagePan=new JPanel();
            imagePan.setLayout(new FlowLayout(FlowLayout.LEFT));
            imagePan.add(new ImagePanel(new File(imagePath),250,true));
            add(imagePan);
            add(Box.createRigidArea(new Dimension(0,5)));
        }

        if(!message.getShareLink().isEmpty()){
            JPanel sharePan=new JPanel();
            sharePan.setLayout(new FlowLayout(FlowLayout.LEFT));
            JLabel lbl=new JLabel("Shared link: ");
            lbl.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
            sharePan.add(lbl);
            JButton linkBtn=new JButton("Click to open in browser");
            linkBtn.setFont(new Font(Font.SANS_SERIF,Font.PLAIN,12));
            linkBtn.addActionListener(l->{
                try{
                    Desktop.getDesktop().browse(new URI(message.getShareLink()));
                }catch(IOException|URISyntaxException e){
                    e.printStackTrace();
                }
            });
            sharePan.add(linkBtn);
            add(sharePan);
        }

        if(message.getVideos().size()>0){
            JPanel lblPan=new JPanel();
            lblPan.setLayout(new FlowLayout(FlowLayout.LEFT));
            JLabel lbl=new JLabel("Videos: ");
            lbl.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
            lblPan.add(lbl);
            add(lblPan);
        }
        for(Message.VideoStore video:message.getVideos()){
            JPanel vidPan=new JPanel();
            vidPan.setLayout(new FlowLayout(FlowLayout.LEFT));
            vidPan.add(new ImagePanel(new File(video.getThumbUri()),250,new File(video.getVideoUri())));
            add(vidPan);
            add(Box.createRigidArea(new Dimension(0,5)));
        }

        if(message.getFilePaths().size()>0){
            JPanel lblPan=new JPanel();
            lblPan.setLayout(new FlowLayout(FlowLayout.LEFT));
            JLabel lbl=new JLabel("Files: ");
            lbl.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
            lblPan.add(lbl);
            add(lblPan);
        }
        for(String filePath:message.getFilePaths()){
            JPanel filePan=new JPanel();
            filePan.setLayout(new FlowLayout(FlowLayout.LEFT));
            JButton linkBtn=new JButton("Click to view file: "+new File(filePath).getName());
            linkBtn.setFont(new Font(Font.SANS_SERIF,Font.PLAIN,12));
            linkBtn.addActionListener(l->{
                try{
                    Desktop.getDesktop().open(new File(filePath));
                }catch(IOException e){
                    e.printStackTrace();
                }
            });
            filePan.add(linkBtn);
            add(filePan);
            add(Box.createRigidArea(new Dimension(0,5)));
        }


        if(!message.getReactString().isEmpty()){
            JPanel reactPan=new JPanel();
            reactPan.setLayout(new FlowLayout(FlowLayout.LEFT));
            JLabel lbl=new JLabel("Reacts: ");
            lbl.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
            reactPan.add(lbl);
            JTextArea reactListArea=new JTextArea(message.getReactString());
            reactListArea.setFont(new Font(Font.SANS_SERIF,Font.PLAIN,12));
            reactListArea.setLineWrap(true);
            reactListArea.setWrapStyleWord(true);
            reactListArea.setOpaque(false);
            reactListArea.setEditable(false);
            reactListArea.setColumns(60);
            reactPan.add(reactListArea);
            add(reactPan);
        }
    }
}
