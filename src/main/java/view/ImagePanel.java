package view;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImagePanel extends JPanel{

    private BufferedImage image;
    private double aspectRatio;
    private final int height;

    public ImagePanel(File imageFile,int height,boolean clickable) {
        this(imageFile,height,clickable,imageFile);
    }

    public ImagePanel(File imageFile,int height,File clickFile){
        this(imageFile,height,true,clickFile);
    }

    private ImagePanel(File imageFile,int height,boolean clickable,File clickFile){
        aspectRatio=1;
        try {
            image = ImageIO.read(imageFile);
            aspectRatio=(double)image.getWidth()/(double)image.getHeight();
        } catch (IOException ex) {
            System.err.println("Some issue loading image : "+imageFile.getAbsolutePath());
        }
        int maxWidth=(int) (((double)Toolkit.getDefaultToolkit().getScreenSize().width)*0.35);
        if((int)(aspectRatio*height)>maxWidth){
            height=(int) (((double)maxWidth)/aspectRatio);
        }
        setPreferredSize(new Dimension((int)(aspectRatio*height),height));

        this.height=height;

        if(clickable){
            addMouseListener(new MouseAdapter(){
                @Override
                public void mousePressed(MouseEvent me){
                    if(me.getButton()==1){
                        try{
                            Desktop.getDesktop().open(clickFile);
                        }catch(IOException e){
                            e.printStackTrace();
                        }
                    }
                }
            });
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0,(int)(aspectRatio*height),height, this); // see javadoc for more info on the parameters
    }

}