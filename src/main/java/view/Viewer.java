package view;

import chat.Chat;
import chat.Message;
import init.StartPoint;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class Viewer extends JFrame{
    private final JLabel currentChatTitle;
    private JPanel panMessages;
    private final Dimension screenSize;

    public Viewer(){
        screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setPreferredSize(new Dimension((int)(screenSize.width*0.6),(int)(screenSize.height*0.85)));
        getContentPane().setLayout(new BoxLayout(getContentPane(),BoxLayout.X_AXIS));

        JPanel panChats=new JPanel();
        panChats.setPreferredSize(new Dimension((int)(screenSize.width*0.15),(int)(screenSize.height*0.8)));
        add(panChats);

        add(Box.createRigidArea(new Dimension(10,0)));

        panMessages=new JPanel();
        JPanel panRight=new JPanel();
        panRight.setLayout(new BoxLayout(panRight,BoxLayout.Y_AXIS));
        panRight.setPreferredSize(new Dimension((int)(screenSize.width*0.45),(int)(screenSize.height*0.8)));

        currentChatTitle=new JLabel();
        JPanel titlePan=new JPanel();
        titlePan.setLayout(new FlowLayout(FlowLayout.LEFT));
        currentChatTitle.setFont(new Font(Font.SANS_SERIF,Font.BOLD,16));
        titlePan.add(currentChatTitle);

        panRight.add(titlePan);
        panRight.add(panMessages);
        add(panRight);

        setupChatPanel(panChats);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        SwingUtilities.invokeLater(()->{
            toFront();
            repaint();
        });
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void setupChatPanel(JPanel panBase){
        JPanel inner=new JPanel();
        JScrollPane scrollPane=new JScrollPane(inner);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.setPreferredSize(new Dimension((int)(screenSize.width*0.15),(int)(screenSize.height*0.8)));
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        inner.setLayout(new BoxLayout(inner,BoxLayout.Y_AXIS));
        Collections.sort(StartPoint.getChats());
        Collections.reverse(StartPoint.getChats());
        for(Chat chat:StartPoint.getChats()){
            JButton btn=new JButton(chat.getTitle());
            btn.setFont(btn.getFont().deriveFont(12f));
            btn.setMinimumSize(new Dimension((int)(screenSize.width*0.15),25));
            btn.setPreferredSize(new Dimension((int)(screenSize.width*0.15),30));
            btn.addActionListener(l->{
                setupMessagePanel(chat);
                currentChatTitle.setText(chat.getTitle());
            });
            if(chat.getSection()==Chat.Section.ARCHIVE){
                btn.setForeground(Color.RED);
            }
            inner.add(btn);
            inner.add(Box.createRigidArea(new Dimension(0,5)));
        }
        panBase.add(scrollPane);
    }

    private void setupMessagePanel(Chat chat){
        List<Message> messages=chat.getMessages();
        AtomicBoolean loadScrolling=new AtomicBoolean(true);
        panMessages.removeAll();
        JPanel inner=new JPanel();
        JScrollPane scrollPane=new JScrollPane(inner);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setPreferredSize(new Dimension((int)(screenSize.width*0.44),(int)(screenSize.height*0.78)));
        inner.setLayout(new BoxLayout(inner,BoxLayout.Y_AXIS));
        panMessages.add(scrollPane);

        int maxMessagesPerLoad=200;
        final int[] messagesLoaded={Math.min(maxMessagesPerLoad, messages.size())};

        for(int i=0; i<messagesLoaded[0]; i++){
            MessageBox innerBox=new MessageBox(messages.get(i));
            JPanel container=new JPanel();
            container.add(innerBox);
            container.setLayout(new FlowLayout(FlowLayout.CENTER));
            container.setMinimumSize(new Dimension((int)(innerBox.getPreferredSize().width*1.2),innerBox.getPreferredSize().height));
            inner.add(container);
            inner.add(Box.createRigidArea(new Dimension(0,10)));
        }

        SwingUtilities.invokeLater(()->{
            scrollPane.getVerticalScrollBar().setValue(0);
            loadScrolling.set(false);
        });
        pack();
        revalidate();

        scrollPane.getVerticalScrollBar().addAdjustmentListener(e->{
            if(((double)scrollPane.getVerticalScrollBar().getValue())/((double)scrollPane.getVerticalScrollBar().getMaximum())>0.9
                    &&messagesLoaded[0]<messages.size()&&
                    !loadScrolling.get()){
                loadScrolling.set(true);
                int currentLocation=scrollPane.getVerticalScrollBar().getValue();

                //do some more loading
                int currentlyLoaded=messagesLoaded[0];
                messagesLoaded[0]=Math.min(messagesLoaded[0]+maxMessagesPerLoad,messages.size());
                for(int i=currentlyLoaded;i<messagesLoaded[0];i++){
                    MessageBox innerBox=new MessageBox(messages.get(i));
                    JPanel container=new JPanel();
                    container.add(innerBox);
                    container.setLayout(new FlowLayout(FlowLayout.CENTER));
                    container.setMinimumSize(new Dimension((int)(innerBox.getPreferredSize().width*1.2),innerBox.getPreferredSize().height));
                    inner.add(container);
                    inner.add(Box.createRigidArea(new Dimension(0,10)));
                }

                SwingUtilities.invokeLater(()->{
                    scrollPane.getVerticalScrollBar().setValue(currentLocation);
                    loadScrolling.set(false);
                });
            }
        });
    }
}
