package data;

import chat.Chat;
import chat.Message;

import java.io.PrintWriter;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class PerChat{
    public PerChat(Chat chat){
        PrintWriter writer=DataCentral.createWriter(chat);
        writer.println("Name,Words,Type,Reacts,Date,Hour");
        DateTimeFormatter dateformatter=DateTimeFormatter.ofPattern("yyyy/MM/dd").withZone(ZoneId.systemDefault());
        DateTimeFormatter hourformatter=DateTimeFormatter.ofPattern("HH").withZone(ZoneId.systemDefault());

        for(Message message:chat.getMessages()){
            String name=message.getSender();
            int words=words(message);
            String type=message.getType().toString().toLowerCase();
            int reacts=message.getReacts().size();
            String date=dateformatter.format(message.getTime());
            String hour=hourformatter.format(message.getTime());
            writer.println(name+","+words+","+type+","+reacts+","+date+","+hour);
        }

        writer.flush();
        writer.close();
    }

    private int words(Message message){
        if(message.getContent().isEmpty()){
            return 0;
        }else{
            return message.getContent().split("[ ./,!?&]").length;
        }
    }
}
