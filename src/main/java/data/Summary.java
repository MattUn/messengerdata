package data;

import chat.Chat;
import chat.Message;
import init.StartPoint;

import java.io.PrintWriter;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Summary{
    public Summary(){
        PrintWriter writer=DataCentral.createWriter(null);
        writer.println("chat,date,actor,count,combined participants,location");
        DateTimeFormatter dtformatter=DateTimeFormatter.ofPattern("yyyy/MM/dd").withZone(ZoneId.systemDefault());

        for(Chat chat: StartPoint.getChats()){
            String chatName=chat.getTitle();
            if(chatName.contains(",")){
                chatName="\""+chatName+"\"";
            }

            List<PersonType> selfList=new ArrayList<>();
            List<PersonType> otherList=new ArrayList<>();
            List<String> selfDateList=new ArrayList<>();
            List<String> otherDateList=new ArrayList<>();
            for(Message message:chat.getMessages()){
                String msgDate=dtformatter.format(message.getTime());
                if(message.isUser()){
                    selfDateList.add(msgDate);
                }else{
                    otherDateList.add(msgDate);
                }
            }
            Set<String> uniqueSetSelf = new HashSet<>(selfDateList);
            for(String temp:uniqueSetSelf){
                selfList.add(new PersonType(temp, Collections.frequency(selfDateList, temp)));
            }
            Set<String> uniqueSetOther = new HashSet<>(otherDateList);
            for(String temp:uniqueSetOther){
                otherList.add(new PersonType(temp, Collections.frequency(otherDateList, temp)));
            }

            for(PersonType person:selfList){
                writer.println(chatName+","+person.date+",self,"+person.count+","+chat.getCombinedParticipants().size()+","+chat.getSection().toString().toLowerCase());
            }
            for(PersonType person:otherList){
                writer.println(chatName+","+person.date+",other,"+person.count+","+chat.getCombinedParticipants().size()+","+chat.getSection().toString().toLowerCase());
            }
        }

        writer.flush();
        writer.close();


    }
    static class PersonType{
        private final String date;
        private final int count;
        public PersonType(String date,int count){
            this.date=date;
            this.count=count;
        }
    }
}
