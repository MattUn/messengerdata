package data;

import chat.Chat;
import init.StartPoint;
import util.LoadingPanel;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class DataCentral{
    public static void runData(){
        LoadingPanel wait=new LoadingPanel("Processing data to file",StartPoint.getChats().size()+1);

        new Thread(()->{//LoadingPanel doesn't render if this runs in main thread. Don't know why yet.
            int fileNum=1;
            new Summary();
            wait.updateBar(1);
            for(Chat chat:StartPoint.getChats()){
                new PerChat(chat);
                fileNum++;
                wait.updateBar(fileNum);
            }
            wait.dispose();
        }).start();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    static PrintWriter createWriter(Chat chat){
        String dir=StartPoint.getMsgDir().getAbsolutePath()
                .substring(0,StartPoint.getMsgDir().getAbsolutePath().length()-8);
        DateTimeFormatter dtformatter=DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault());
        dir=dir+"MessengerData_"+dtformatter.format(Instant.now());//top level directory to store everything in
        new File(dir).mkdir();

        if(chat==null){
            dir=dir+File.separator+"md_summary.csv";
        }else{
            dir=dir+File.separator+chat.getSection().toString().toLowerCase();
            new File(dir).mkdir();

            String chatName;
            if(chat.getTitle()!=null&&!chat.getTitle().isEmpty()) {
                chatName = chat.getTitle();
            }else{
                Random random=new Random();
                chatName="UNKNOWN"+random.nextInt();
            }
            chatName=chatName.replace('/','_');
            chatName=chatName.replace('\\','_');
            chatName=chatName.replace(':','_');
            chatName=chatName.replace('*','_');
            chatName=chatName.replace('?','_');
            chatName=chatName.replace('"','_');
            chatName=chatName.replace('<','_');
            chatName=chatName.replace('>','_');
            chatName=chatName.replace('|','_');
            dir=dir+File.separator+"md_"+chatName+".csv";
        }

        PrintWriter writer=null;
        try{
            writer=new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(dir), StandardCharsets.UTF_8)));
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        return writer;
    }
}
