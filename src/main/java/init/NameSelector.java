package init;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class NameSelector extends JDialog{
    private String selectedName=null;
    public NameSelector(){
        setModalityType(ModalityType.APPLICATION_MODAL);
        JLabel lblInstruction=new JLabel("Enter your name as it appears on Messenger");

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setPreferredSize(new Dimension((int)(screenSize.width*0.2),(int)(screenSize.height*0.5)));


        DefaultListModel<String> listModel=new DefaultListModel<>();
        for(String name:StartPoint.getAllParticipants()){
            listModel.addElement(name);
        }
        JList<String> nameList=new JList<>(listModel);
        nameList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        nameList.setSelectedIndex(0);
        JScrollPane listScroll=new JScrollPane(nameList);
        listScroll.setPreferredSize(new Dimension((int)(screenSize.width*0.2),(int)(screenSize.height*0.45)));

        JTextField fieldName=new JTextField();
        fieldName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e){
                onTextChange();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
                onTextChange();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                onTextChange();
            }

            public void onTextChange() {//no longer a guaranteed order to list but much more responsive
                for(String name:StartPoint.getAllParticipants()){
                    if(!name.toLowerCase().contains(fieldName.getText().toLowerCase())){
                        listModel.removeElement(name);
                    }else if(!listModel.contains(name)){
                        listModel.addElement(name);
                    }
                }
                nameList.setSelectedIndex(0);
                nameList.ensureIndexIsVisible(0);
            }
        });

        JButton butOk=new JButton("OK");
        butOk.addActionListener(e -> {
                selectedName=nameList.getSelectedValue();
                dispose();
            }
        );
        fieldName.addActionListener(e -> {
                selectedName=nameList.getSelectedValue();
                dispose();
            }
        );

        getContentPane().setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
        add(lblInstruction);
        add(fieldName);
        add(butOk);
        add(listScroll);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public String getSelectedName(){
        return selectedName;
    }
}
