package init;

import data.DataCentral;
import view.Viewer;

import javax.swing.*;
import java.awt.*;

public class DataViewSwitch extends JDialog{
    public DataViewSwitch(){
        setModalityType(ModalityType.APPLICATION_MODAL);

        JButton btnView=new JButton("View all chats and messages");
        btnView.setFont(btnView.getFont().deriveFont(16f));
        btnView.addActionListener(e -> {
            dispose();
            new Viewer();
        });

        JButton btnData=new JButton("Produce data files for Power BI visualisations");
        btnData.setFont(btnData.getFont().deriveFont(16f));
        btnData.addActionListener(e -> {
            dispose();
            DataCentral.runData();
        });

        getContentPane().setLayout(new GridLayout(2,1));
        add(btnView);
        add(btnData);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
}
