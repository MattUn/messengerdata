package init;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import util.LoadingPanel;

import javax.swing.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Input{

    public static List<List<JsonObject>> readAllMessages(File msgDir){
        //outer list stores all chats
        List<List<JsonObject>> outerList=new ArrayList<>();
        //inner list stores all json files for a chat

        List<File> chatList=new ArrayList<>();
        try{
            chatList.addAll(Arrays.asList(Objects.requireNonNull(new File(msgDir.getAbsolutePath()+File.separator+"inbox").listFiles())));
            chatList.addAll(Arrays.asList(Objects.requireNonNull(new File(msgDir.getAbsolutePath()+File.separator+"archived_threads").listFiles())));
        }catch(NullPointerException ex){
            String message="<html>Could not find inbox and archived_messages folders within messages<br>" +
                    "Check files and directories are present and relaunch</html>";
            JOptionPane.showMessageDialog(new JFrame(),message,"Selection issue",JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }

        LoadingPanel wait=new LoadingPanel("Loading your messages from "+chatList.size()+" chats",chatList.size());

        int fileNum=0;
        for(File chatDir:chatList){
            List<JsonObject> innerList=new ArrayList<>();
            for(File file: Objects.requireNonNull(chatDir.listFiles())){
                if(file.getAbsolutePath().endsWith("json")){
                    innerList.add(readMessage(file));
                }
            }
            outerList.add(innerList);
            fileNum++;
            wait.updateBar(fileNum);
        }

        wait.dispose();
        return outerList;
    }

    private static JsonObject readMessage(File jsonFile){
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(jsonFile), StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.fromJson(br,JsonObject.class);
    }
}
