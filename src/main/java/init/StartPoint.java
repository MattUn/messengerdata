package init;

import chat.Chat;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import data.DataCentral;
import mdlaf.MaterialLookAndFeel;
import mdlaf.themes.MaterialLiteTheme;
import util.EncodingFix;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.*;

public class StartPoint{
    private static File msgDir;
    private static String userName;
    private static List<Chat> chats;
    private static Set<String> allParticipants;
    private static boolean cli=false;

    public static void main(String[] args){
        try{
            MaterialLookAndFeel material = new MaterialLookAndFeel(new MaterialLiteTheme());
            UIManager.setLookAndFeel(material);
        }catch(UnsupportedLookAndFeelException e){
            e.printStackTrace();
        }

        if(args.length==2&&args[0].equalsIgnoreCase("cli")){
            cli=true;
        }

        if(cli){
            File dir=new File(args[1]);
            if(args[1].endsWith("messages")&&dir.exists()){
                msgDir=dir;
            }else{
                System.out.println("Directory wasn't valid");
                System.exit(1);
            }
        }else{
            msgDir=selectMessagesDir();
        }

        chats=new ArrayList<>();
        List<List<JsonObject>> allMessages=Input.readAllMessages(msgDir);
        findAllParticipants(allMessages);

        if(cli){
            Scanner scanner=new Scanner(new InputStreamReader(System.in, StandardCharsets.UTF_8));
            System.out.println("Enter your name as it appears on Messenger");
            userName=scanner.nextLine();
            scanner.close();
        }else{
            NameSelector nameSelector=new NameSelector();
            userName=nameSelector.getSelectedName();
        }


        initChats(allMessages);

        if(cli){
            DataCentral.runData();
        }else{
            new DataViewSwitch();
        }
    }

    private static File selectMessagesDir(){
        File selectedFile;
        Boolean old = UIManager.getBoolean("FileChooser.readOnly");
        UIManager.put("FileChooser.readOnly", Boolean.TRUE);
        JFileChooser jfc = new JFileChooser();
        jfc.setPreferredSize(new Dimension(jfc.getPreferredSize().width+50,jfc.getPreferredSize().height+100));
        UIManager.put("FileChooser.readOnly", old);
        jfc.setDialogTitle("Select messages folder");
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        jfc.setMultiSelectionEnabled(false);
        jfc.setAcceptAllFileFilterUsed(false);
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue != JFileChooser.APPROVE_OPTION) {//cancel selected
            System.exit(1);
        }
        if(jfc.getSelectedFile().getAbsolutePath().endsWith("messages")){
            selectedFile=jfc.getSelectedFile();
        }else if(jfc.getCurrentDirectory().getAbsolutePath().endsWith("messages")){
            selectedFile=jfc.getCurrentDirectory();
        }else{
            String message="<html>You must select a folder called messages<br>Try again</html>";
            JOptionPane.showMessageDialog(new JFrame(),message,"Selection issue",JOptionPane.WARNING_MESSAGE);
            return selectMessagesDir();
        }
        return selectedFile;
    }

    private static void initChats(List<List<JsonObject>> chatsJson){
        for(List<JsonObject> chatJson:chatsJson){
            Chat chat=new Chat(chatJson.get(0));
            for(int i=1;i<chatJson.size();i++){
                chat.addExtraFile(chatJson.get(i));
            }
            chats.add(chat);
        }
    }

    private static void findAllParticipants(List<List<JsonObject>> chatsJson){
        allParticipants=new TreeSet<>();
        for(List<JsonObject> chat:chatsJson){
            JsonArray jsonParticipants=chat.get(0).get("participants").getAsJsonArray();
            for(JsonElement person:jsonParticipants){
                allParticipants.add(EncodingFix.fixEncoding(person.getAsJsonObject().get("name").getAsString()));
            }
        }
    }

    public static File getMsgDir(){
        return msgDir;
    }

    public static String getUserName(){
        return userName;
    }

    public static List<Chat> getChats(){
        return chats;
    }

    public static Set<String> getAllParticipants(){
        return allParticipants;
    }

    public static boolean isGui(){
        return !cli;
    }
}
