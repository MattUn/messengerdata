package util;

import init.StartPoint;

import javax.swing.*;
import java.awt.*;

public class LoadingPanel extends JFrame{
    private final JProgressBar loadBar;
    private int cliMax;
    private int cliCurrent;
    public LoadingPanel(String message, int maxBar){
        if(StartPoint.isGui()){
            getContentPane().setLayout(new GridLayout(2, 0));
            JLabel msgLabel=new JLabel(" "+message+" ");
            msgLabel.setFont(msgLabel.getFont().deriveFont(26.0f));
            getContentPane().add(msgLabel);
            loadBar=new JProgressBar();
            loadBar.setMinimum(0);
            loadBar.setMaximum(maxBar);
            loadBar.setValue(0);
            getContentPane().add(loadBar);
            pack();
            setLocationRelativeTo(null);
            setVisible(true);
            setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        }else{
            loadBar=null;
            System.out.println(message+": __________");
            for(int i=0;i<message.length();i++){
                System.out.print(" ");
            }
            System.out.print("  ");
            cliMax=maxBar;
            cliCurrent=0;
        }
    }

    public void updateBar(int newPosition){
        if(StartPoint.isGui()){
            if(newPosition>loadBar.getMaximum()||newPosition<0){
                throw new IllegalArgumentException();
            }
            loadBar.setValue(newPosition);
        }else{
            if(newPosition>cliMax||newPosition<0){
                throw new IllegalArgumentException();
            }
            int cliNew=(newPosition*10)/cliMax;
            for(int i=0;i<cliNew-cliCurrent;i++){
                System.out.print("*");
            }
            cliCurrent=cliNew;
            if(cliNew==10){
                System.out.println();
            }
        }
    }
}
