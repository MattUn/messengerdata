package util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

/**
 * Solution for fixing Facebook encoding by Ondrej Sotolar.
 * Very small modifications to their code were made.
 * @see <a href="https://stackoverflow.com/a/57867459/4257776">stackoverflow.com/a/57867459/4257776</a>
 */
public class EncodingFix{
    public static String fixEncoding(String input){
        String unescaped = unescapeMessenger(input);
        byte[] bytes = unescaped.getBytes(StandardCharsets.ISO_8859_1);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    private static String unescapeMessenger(String str) {
        if (str == null) {
            return null;
        }
        try {
            StringWriter writer = new StringWriter(str.length());
            unescapeMessenger(writer, str);
            return writer.toString();
        } catch (IOException ioe) {
            // this should never ever happen while writing to a StringWriter
            ioe.printStackTrace();
        }
        return null;
    }

    private static void unescapeMessenger(Writer out, String str) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("The Writer must not be null");
        }
        if (str == null) {
            return;
        }
        int sz = str.length();
        StringBuilder unicode = new StringBuilder(4);
        boolean hadSlash = false;
        boolean inUnicode = false;
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);
            if (inUnicode) {
                unicode.append(ch);
                if (unicode.length() == 4) {
                    // unicode now contains the four hex digits
                    // which represents our unicode character
                    try {
                        int value = Integer.parseInt(unicode.toString(), 16);
                        out.write((char) value);
                        unicode.setLength(0);
                        inUnicode = false;
                        hadSlash = false;
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                        System.out.println("Can't parse: "+unicode);
                    }
                }
                continue;
            }
            if (hadSlash) {
                hadSlash = false;
                if (ch == 'u') {
                    inUnicode = true;
                } else {
                    out.write("\\");
                    out.write(ch);
                }
                continue;
            } else if (ch == '\\') {
                hadSlash = true;
                continue;
            }
            out.write(ch);
        }
        if (hadSlash) {
            // then we're in the weird case of a \ at the end of the
            // string, let's output it anyway.
            out.write('\\');
        }
    }
}
