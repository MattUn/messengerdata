# MessengerData
A program for viewing and analysing Facebook Messenger chats.

Download your information at www.facebook.com/dyi in json format.

Analysis is done using Power BI Desktop. To use the supplied pbix files, 
edit the query then change the source to the relevant csv.

To run the application, use Gradle task "run" or 
build the executable jar with task "shadowJar".
Alternatively, download the executable jar from the releases page.

You can optionally create the csv files purely from the command line. 
Use arguments "cli" and your absolute/relative messages directory.